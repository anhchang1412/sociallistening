import React from 'react';
import { Row, Input, Typography } from 'antd';
import axios from 'axios';

import openNotification from '../helper/notification';
import Layout from '../components/common/Layout';

const Add = () => {
  const domain = '';
  const handle = async (url) => {
    openNotification('Thông báo', 'Bài viết đang trong quá trình phân tích. Vui Lòng Đợi!', 'info');
    await axios({ url: `${domain}/${url}` });
  };

  return (
    <Layout>
      <Row style={{ paddingTop: 100 }}>
        <Typography.Title level={3}>Thêm bài mới</Typography.Title>
        <Input.Search
          placeholder="Link Post"
          enterButton="GET"
          size="large"
          onSearch={(url) => handle(url)}
        />
      </Row>
    </Layout>
  );
};

export default Add;
